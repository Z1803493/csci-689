/******************************************************
CSCI 689 - Assignment 7 - Semester (Fall) 2016
Program:        Date.h
Author:         Patlolla, Akhil Reddy
Zid:            z1803493
Section:        1
Instructor:     Ibrahim Onyuksel
TA:             Anthony Schroeder
TA:             Avinash Chirumamilla
Date Due:       Wednesday, October 19, 2016
Purpose:        A header file for date class implementation
                extension for phase 2
*******************************************************/
#ifndef PROG_H
#define PROG_H


#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;


class Date
{
    /*  operator overload to take input */
    friend istream & operator>>(istream&, Date &);
    /* oeprator overloading for output */
    friend ostream & operator<<(ostream&,const Date &);
private:
    string month;
    int Day;
    int Year;
    /*check value of month*/
    bool isValidMonth(void)const;
    /*state days in month*/
    int daysInMonth(const string &) const;
    /*check the leap year*/
    bool isLeapYear(void) const;
    /* used to return index value of the month in ventor */
    int monthIndex() const;

public:
    /*constructor*/
    Date(const string & m ="January",int d=1 ,int y=2000 ):month(m),Day(d),Year(y) {
        Month();
    }
    /* copy constructor */
    Date( const Date & d);
    /* Assignment operator overload */
    Date& operator=(const Date& d);
    /*sets value of month*/
    void setMonth(const string m); 
    /*sets value of day*/
    void setDay(const int d); 
    /*sets value of Year*/
    void setYear(const int y);
    /*get  value of month*/
    string getMonth()const ;
    /*get value of day*/
    int getDay()const ;
    /*get value of year*/
    int getYear()const;
    /*captilize value ofmonth*/
    void Month(void);
    /*check for valid date*/
    bool isValidDate(void) const;
    /*convert valid date to string*/
    string toString(void) ;
    /* method to add days to calling objects date   */
    Date& addDay (const int& );
    /* method to add month to calling objects date  */
    Date& addMonth (const int& );
    /* method to add years to calling objects date  */
    Date& addYear(const int& );
    /* method which calculates the number of days from the reference */
    int dayIndex() const;

};
/* method to convert int to string */
string intToString(int );
/* method calculating the difference in dates*/
unsigned int dateDiff(const Date& d1 , const Date& d2);

#endif