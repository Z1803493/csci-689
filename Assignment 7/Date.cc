/******************************************************
CSCI 689 - Assignment 7 - Semester (Fall) 2016
Program:        Date.cc
Author:         Patlolla, Akhil Reddy
Zid:            z1803493
Section:        1
Instructor:     Ibrahim Onyuksel
TA:             Anthony Schroeder
TA:             Avinash Chirumamilla
Date Due:       Wednesday, October 19, 2016
Purpose:        A date class implementation with extention
*******************************************************/
#include "Date.h"

/* Copy constructor */
Date:: Date( const Date & d){
    Day=d.Day;month=d.month;Year=d.Year;
}

/* Operator overloading Assignment */
Date&Date::operator=(const Date& d){

    /* self assignment  */
    if (this == &d) return *this;
    Day = d.Day;
    month = d.month;
    Year = d.Year;
    return *this;
}

/* method to add day to calling objects date  */
Date& Date::addDay(const int& n){
    string local_months [12] =  { "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
    int counter;
    if(n>0)
    {
        for(int i=0; i<12; i++){
            if(month == local_months[i])
                counter = i;
        }
        bool flag;
        int flag_days = Day;
        flag_days += n;

        do
        {
            flag = false;
            if(flag_days>daysInMonth(month)){
                flag_days-=daysInMonth(month);
                counter++;
                month=local_months[counter];
                flag = true;
            }
            if(counter > 11){
                counter=0;
                month=local_months[counter];
                Year++;
            }
        }
        while(flag);
        Day = flag_days;
    }
    if(n<0)
    {
        for(int i=0; i<12; i++){
            if(month == local_months[i])
                counter = i;
        }
        bool flag;
        int flag_days = Day;
        flag_days += n;
        do
        {
            flag = false;
            if(flag_days<0){
                flag_days+=daysInMonth(month);
                counter=11-counter;
                month=local_months[counter];
                flag = true;
            }
            if(counter < 0){
                counter=11;
                month=local_months[counter];
                cout<<month;
                Year--;
            }
        }
        while(flag);
        Day = flag_days;
    }
    return *this;
}

/* method to add month to calling objects date  */
Date& Date :: addMonth (const int& n){
    string local_months [12] =  { "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
    if(n>0)
    {
        int counter;
        for(int i=0; i<12; i++){
            if(month==local_months[i])
                counter = i;
        }

        if((counter+n)<12){
            counter=counter+n;
            month=local_months[counter];
        }
        else
        {
            counter =0;
            month=local_months[counter];
            counter = counter+(n);
            if(counter<12){
                counter=counter%12;
                month=local_months[counter];
                Year++;
            }
            else{
                counter=counter%12;
                month=local_months[counter];
                Year = Year + (n/12);
            }
        }
        if(Day>daysInMonth(month)){
            Day=1;
            counter++;
            month=local_months[counter];
        }
    }
    if(n<0){
        int counter;
        for(int i=0; i<12; i++){
            if(month==local_months[i])
                counter = i;
        }
        if((counter-1)>=0){
            counter=counter+(n);
            month=local_months[counter];
        }
        else{
            counter =11;
            month=local_months[counter];
            counter = counter+(n+1);
            if(counter>-12){
                counter=counter%12;
                month=local_months[counter];
                Year--;
            }
            else{
                counter=counter%12;
                counter=(counter)*(-1);
                month=local_months[counter];
                Year = Year +(n/12);
            }

        }
        if(Day>daysInMonth(month)){
            Day=1;
            counter++;
            month=local_months[counter];
        }
    }
    return *this;
}

/* method to add years to calling objects date  */
Date& Date::addYear(const int& n){
    int tempyear=0;
    if(n>0){
        tempyear = Year + n;
        Year = tempyear;
        if (tempyear == 0){
            tempyear = 1;
            Year=tempyear;
        }
        if (!(isLeapYear())){
            if (Day == 29 && month == "February"){
                Day = 1;
                month = "March";
            }
        }
    }
    if(n<0){
        tempyear = Year + n;
        if (tempyear == 0)tempyear = -1;
        Year = tempyear;
        if (!(isLeapYear())){
            if (month == "February"&&Day == 29  ){
                Day = 1;
                month = "March";
            }
        }
    }
    return *this;
}

/* method which calculates the number of days from the reference */
int Date::dayIndex() const{
    const vector<string> months = { "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
    int temp=0;
    int mon,i;
    Date d = (*this);
    if (d.Year > 0){
        for (i = 1; i < Year; i++){
            d.setYear(i);
            if (d.isLeapYear())
                temp += 366;
            else
                temp += 365;
        }
        d.setYear(i);
        mon = d.monthIndex();
        for (int i = 0; i < mon; i++){
            d.month = months[i];
            temp += d.daysInMonth(d.month);
        }
        temp += d.Day;
    }
    if(d.Year<0){
        int mon;
        for (int i = 1; i < ((d.Year)*-1); i++){
            d.setYear(i);
            if (d.isLeapYear())
                temp += 1;
            else
                temp += 365;
        }
        d.setYear(i);
        mon = d.monthIndex();
        for (int i = 0; i < mon; i++){
            d.month = months[i];
            temp += d.daysInMonth(d.month);
        }
        temp += d.Day;
        temp=-(temp);
    }
    if ((Day == 1) && (month == "January") && (Year == 1))
        return 1;
    return temp;

}

/* used to return index value of the month in ventor */
int Date::monthIndex() const{
    const vector<string> months = { "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
    int ind = 0;
    for (unsigned i = 0; i < months.size(); i++){
        if (month == months[i])
            ind = i;
    }
    return ind;
}
/* method to calcualte date difference */
unsigned int  dateDiff (const Date& d1, const Date& d2) {
    int dated1 = 0, dated2 = 0;
    unsigned diff = 0;
    dated1 = d1.dayIndex();
    cout<<dated1;
    dated2 = d2.dayIndex();
    cout<<dated2;
    if (dated1 > dated2)
        diff = dated1 - dated2;
    else
        diff = dated2 - dated1;

    if (((d1.getYear() < 0) && (d2.getYear() > 0)) || ((d1.getYear() > 0) && (d2.getYear() < 0)))
        diff = diff - 1;
    return diff;
}


/*  sets value of month */
void Date::setMonth(const string m){
    month = m;
    Month();
}

/* sets value of day */
void Date::setDay(const int d){
    Day = d;
}
/*  sets value of year */
void Date::setYear(const int y){
    Year = y;
}
/*  return value of month */
string Date::getMonth() const{
    return month;
}
/* return value of day */
int Date::getDay() const{
    return Day;
}
/* return value of year  */
int Date::getYear() const{
    return Year;
}
/* capatilize the month value */
void Date::Month(void){
    bool cap = true;
    for (unsigned int i = 0; i <= month.length(); i++){
        if (i == 0) {
            if (isalpha(month[i]) && cap == true) {
                month[i] = toupper(month[i]);
                cap = false;
            }
            else if (isspace(month[i])) {
                cap = true;
            }
        }
        else {
            month[i] = tolower(month[i]);
        }
    }
}
/* check for valid month */
bool Date::isValidMonth(void) const{

    const vector<string> months = { "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" };
    if (find(months.cbegin(), months.cend(), month) != months.end()) {
        int f = daysInMonth(month);
        if (Day >= 1 && Day <= f) {
            return true;
        }
        else {
            return false;
        }
    }
    else {
        return false;
    }
}
/* state days in month */
int Date::daysInMonth(const string& month) const {
    int counter = 0;
    int i;
    if (month == "January") {
        for (i = 1; i <= 31; i++) {
            counter++;
        }
        return counter;
    }
    else if (month == "February") {
        if (isLeapYear() == true) {
            for (i = 1; i <= 29; i++) {
                counter++;
            }
            return counter;
        }
        else {
            for (i = 1; i <= 28; i++) {
                counter++;
            }
            return counter;
        }
    }
    else if (month == "March") {
        for (i = 1; i <= 31; i++) {
            counter++;
        }
        return counter;
    }
    else if (month == "April") {
        for (i = 1; i <= 30; i++) {
            counter++;
        }
        return counter;
    }
    else if (month == "May") {
        for (i = 1; i <= 31; i++) {
            counter++;
        }
        return counter;
    }
    else if (month == "June") {
        for (i = 1; i <= 30; i++) {
            counter++;
        }
        return counter;
    }
    else if (month == "July") {
        for (i = 1; i <= 31; i++) {
            counter++;
        }
        return counter;
    }
    else if (month == "August") {
        for (i = 1; i <= 31; i++) {
            counter++;
        }
        return counter;
    }
    else if (month == "September") {
        for (i = 1; i <= 30; i++) {
            counter++;
        }
        return counter;
    }
    else if (month == "October") {
        for (i = 1; i <= 31; i++) {
            counter++;
        }
        return counter;
    }
    else if (month == "November") {
        for (i = 1; i <= 30; i++) {
            counter++;
        }
        return counter;
    }
    else if (month == "December") {
        for (i = 1; i <= 31; i++) {
            counter++;
        }
        return counter;
    }
    else {
        return false;
    }
}
/* check for date validity */
bool Date::isValidDate(void) const{
    if (Year == 0) {
        return false;
    }
    if ((isValidMonth()) == true) {
        return true;
    }
    else {
        return false;
    }
}
/* retirn string for valid date*/
string Date::toString(void){
    string da, mo, yea, s, s1;
    da = intToString(Day); 
    yea = intToString(Year);
    if (month == "January") {
        s = "Jan";
        mo = mo + s;
    }
    else if (month == "February") {
        s = "Feb";
        mo = mo + s;
    }
    else if (month == "March") {
        s = "Mar";
        mo = mo + s;
    }
    else if (month == "April") {
        s = "Apr";
        mo = mo + s;
    }
    else if (month == "May") {
        s = "May";
        mo = mo + s;
    }
    else if (month == "June") {
        s = "Jun";
        mo = mo + s;
    }
    else if (month == "July") {
        s = "Jul";
        mo = mo + s;
    }
    else if (month == "August") {
        s = "May";
        mo = mo + s;
    }
    else if (month == "September") {
        s = "Sep";
        mo = mo + s;
    }
    else if (month == "October") {
        s = "Oct";
        mo = mo + s;
    }
    else if (month == "November") {
        s = "Nov";
        mo = mo + s;
    }
    else if (month == "December") {
        s = "Dec";
        mo = mo + s;
    }
    s1 = s1 + da + "-" + mo + "-" + yea;
    return s1;
}
/* check for leap year */
bool Date::isLeapYear(void) const {
    if (Year % 4 == 0 && (Year % 400 == 0 || Year % 100 != 0)) {
        return true;
    }
    else {
        return false;
    }
}
/* to convert string */
string intToString(int d) {
    string s;
    s = s + to_string(d);
    return s;
}
/* oeprator overloading for output */
ostream& operator<<(ostream& os, const Date& d){

    os << d.Day << " " << d.month << ", " << d.Year; 
    return os;
}
/*  operator overload to take input */
istream& operator>>(istream& is, Date& d){

    is >> d.month >> d.Day;
    d.Month();
    char c = is.get();
    c = is.get();
    if (isdigit(c)) {
        is.unget();
        is >> d.Year;
    }
    else {
        is >> d.Year;
    }
    return is;
}