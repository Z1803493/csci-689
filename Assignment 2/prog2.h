/******************************************************
CSCI 689 - Assignment 2 - Semester (Fall) 2016
Program:        prog1.cc
Author:         Patlolla, Akhil Reddy
Zid:            z1803493
Section:        1
Instructor:     Ibrahim Onyuksel
TA:             Anthony Schroeder
TA:             Avinash Chirumamilla
Date Due:       Tuesday, September 6, 2016
Purpose:        A Simple Program to Calculate and print 
                perfect numbers between 1 and 9999
*******************************************************/
#ifndef PROG_H
#define PROG_H


#include <iostream>
#include <string>
using namespace  std;
const int RANGE = 9999;
bool isPerfect(int i);
void divisors(int i);
#endif

