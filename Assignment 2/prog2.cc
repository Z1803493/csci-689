/******************************************************
CSCI 689 - Assignment 2 - Semester (Fall) 2016
Program:        prog1.cc
Author:         Patlolla, Akhil Reddy
Zid:            z1803493
Section:        1
Instructor:     Ibrahim Onyuksel
TA:             Anthony Schroeder
TA:             Avinash Chirumamilla
Date Due:       Tuesday, September 6, 2016
Purpose:        A Simple Program to Calculate and print
                perfect numbers between 1 and 9999
*******************************************************/
#include "prog2.h"

int main(){
        for(int n = 1; n < RANGE; n++){
                if (isPerfect(n) == true){
                        cout<<n<<" = ";
                        divisors(n);
                }
        }
        return 0;
}

//method to calcualte Perfect number
bool isPerfect(int n){
  int sum = 0;
  for (int i = 1; i < n; i++)
        if( n % i == 0)
                sum = sum + i;
  if(sum == n)
         return true;
  else
         return false;

}
//method which converts divisors to string values 
void divisors(int n){
        string master="";
        for (int i = 1; i < n; i++)
                if(n%i==0){
                        master  = master + to_string(i)+" +";
                }
        master.pop_back();
        cout<<master<<endl;
}
