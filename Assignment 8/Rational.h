/******************************************************
CSCI 689 - Assignment 8 - Semester (Fall) 2016
Program:        Rational.h
Author:         Patlolla, Akhil Reddy
Zid:            z1803493
Section:        1
Instructor:     Ibrahim Onyuksel
TA:             Anthony Schroeder
TA:             Avinash Chirumamilla
Date Due:       Thursday, October 27, 2016
Purpose:        A Rational number implementation 
*******************************************************/
#ifndef RATIONAL_H_INCLUDED
#define RATIONAL_H_INCLUDED

#include <iostream>
#include <sstream>

using namespace std;


class Rational{
    private:
        int num, den;
        int gcd(int x, int y) {
            int r = x % y;
            while (r != 0) {
                x = y;
                y = r;
                r = x % y;
            }
            return y;
        }
    public:
        /* This is a constructor create a new Rational object
		with the numerator n and denominator d
		if d = 0,
		it prints out an error message on stderr but it
		doesn’t stop the execution of the program.
		To reduce a Rational number to its lowest terms, it
		calls the private member function: int gcd ( int x, int y )
		that returns the greatest common divisor of the arguments x and y */
       	Rational ( const int& n = 0 , const int& d = 1 );
    	/* This is a public copy constructor is used to create a new
			Rational object from the copy of the Rational object r. */
    	Rational ( const Rational& r );
		/* This is a public assignment operator
		overwrites an existing Rational object with the copy of the Rational object r */
    	Rational& operator = ( const Rational& r );
    	/* These four public member functions overload the operators +=, –
			=, *=, and /= operators, respectively, for the Rational class. */
    	Rational& operator -= ( const Rational& r );
    	Rational& operator *= ( const Rational& r );
    	Rational& operator += ( const Rational& r );
    	Rational& operator /= ( const Rational& r );
    	/* This is a public member
		functions overload the pre-increment (++)
		operator, for the Rational class. */
    	Rational& operator ++ ( );
    	/* This is a public member
		functions overload the pre-decrement ( -- )
		operator, for the Rational class.*/
    	Rational& operator -- ( );
    	/*  These two public member functions overload the post-increment (++) and post-
			decrement ( -- ) operators, respectively, for the Rational class. Note: To
			differentiate between the pre- and post- versions of the operators ( ++ ) and ( -
			- ), an unused argument is included in the post-versions of these operators.*/
    	Rational operator ++ ( int unused );
    	Rational operator -- ( int unused );
    	/* These four friend functions overload the
		arithmetic operators +, –, *, and /, respectively, for the Rational class.*/
    	friend Rational operator + ( const Rational& r1, const Rational& r2 );
    	friend Rational operator - ( const Rational& r1, const Rational& r2 );
    	friend Rational operator * ( const Rational& r1, const Rational& r2 );
    	friend Rational operator / ( const Rational& r1, const Rational& r2 );
    	/*These six friend functions overload the relational operators ==, !=, <, <=, >, and >=,
    	 respectively, for the Rational class. */
    	friend bool operator == ( const Rational& r1, const Rational& r2 );
    	friend bool operator != ( const Rational& r1, const Rational& r2 );
    	friend bool operator < ( const Rational& r1, const Rational& r2 );
    	friend bool operator <= ( const Rational& r1, const Rational& r2);
    	friend bool operator > ( const Rational& r1, const Rational& r2 );
    	friend bool operator >= ( const Rational& r1, const Rational& r2 );
    	/* This friend function overloads the stream insertion operator ( << ) for the Rational class.
    		It can be used to print out the Rational number r with the numerator num and
			denominator den as num/den on the output stream os, and if den = 1, it only
			prints out num.*/
    	friend ostream& operator << ( ostream& os, const Rational& r );
    	/* This friend function overloads the stream extraction operator ( >> ) for the Rational class.
    		It can be used to read the Rational number r from the input stream is, where each Rational
			number is given on a separate line with its three arguments: numerator,
			separator ( / ), and denominator, separated one or more white spaces. If a line
			in is does not contain a valid Rational number, it prints out an error message
			on stderr but it doesn’t stop the execution of the program.*/
    	friend istream& operator >> ( istream& is, Rational& r );
};


#endif // RATIONAL_H_INCLUDED
