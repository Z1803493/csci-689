/******************************************************
CSCI 689 - Assignment 8 - Semester (Fall) 2016
Program:        Rational.cc
Author:         Patlolla, Akhil Reddy
Zid:            z1803493
Section:        1
Instructor:     Ibrahim Onyuksel
TA:             Anthony Schroeder
TA:             Avinash Chirumamilla
Date Due:       Thursday, October 27, 2016
Purpose:        A Rational number implementation 
*******************************************************/
#include "Rational.h"
 
/* This is a constructor create a new Rational object
with the numerator n and denominator d
if d = 0,
it prints out an error message on stderr but it
doesn’t stop the execution of the program.
To reduce a Rational number to its lowest terms, it
calls the private member function: int gcd ( int x, int y )
that returns the greatest common divisor of the arguments x and y */
Rational::Rational(const int& n , const int& d){//:num(n),den(d){
	if(d == 0)
		 cerr <<"Error: division by zero"<<endl;
	else{
		int g = gcd(abs(n), abs(d));
		num = n / g;
		den = abs(d) / g;
	}
	if (d < 0) num = -num;
}
/* This is a public copy constructor is used to create a new
Rational object from the copy of the Rational object r. */
Rational::Rational(const Rational& r){
	num = r. num; den = r.den;
}
/* This is a public assignment operator
overwrites an existing Rational object with the copy of the Rational object r */
Rational& Rational::operator = (const Rational& r){
	if (&r != this){
		num = r.num;
		den = r.den;
	}
	return *this;
}
/* These four friend functions overload the
arithmetic operators +, -, *, and /*/
Rational operator+ ( const Rational& r1, const Rational& r2 ){
	return Rational(r1.num * r2.den + r2.num * r1.den, r1.den * r2.den);
}
Rational operator- ( const Rational& r1, const Rational& r2 ){
	return Rational(r1.num * r2.den - r2.num * r1.den, r1.den * r2.den);
}
Rational operator* ( const Rational& r1, const Rational& r2 ){
	return Rational(r1.num * r2.num, r1.den * r2.den);
}
Rational operator/ ( const Rational& r1, const Rational& r2 ){	
	return Rational(r1.num * r2.den, r1.den * r2.num);

}
/* These six friend functions overload the relational operators
==, !=, <, <=, >, and >=, respectively, for the Rational class. */
bool operator== ( const Rational& r1, const Rational& r2 ){
	if (r1.num != r2.num)
		return false;
	return r1.den==r2.den;
}
bool operator!= ( const Rational& r1, const Rational& r2 ){
	return !(r1==r2);
}
bool operator< ( const Rational& r1, const Rational& r2 ){
	return r1.num * r2.den < r1.den * r2.num;
}
bool operator<= ( const Rational& r1, const Rational& r2){
	return (r1<r2||r1==r2);
}
bool operator> ( const Rational& r1, const Rational& r2 ){
	return !(r1<=r2);
}
bool operator>= ( const Rational& r1, const Rational& r2 ){
	return (r1>r2||r1==r2);
}

/* This is a public member functions overload the operators -=
operator , for the Rational class*/
Rational& Rational::operator-=(const Rational& r){
	*this = *this - r; 
	return *this;
}

/* This is a public member functions overload the operators *=
operator, for the Rational class*/
Rational& Rational::operator*=(const Rational& r){
	*this = *this * r; 
	return *this;
}

/* This is a public member functions overload the operators +=
operator for the Rational class*/
Rational& Rational::operator+=(const Rational& r){
	*this = *this + r; 
	return *this;
}

/* This is a public member functions overload the operators /=
operator, for the Rational class*/
Rational& Rational::operator /= (const Rational& r){
	*this = *this / r; 
	return *this;
}
/* This is a public member
functions overload the pre-increment (++)
operator, for the Rational class. */
Rational& Rational::operator ++ (){
	num += den;	
	return *this;
}

/* This is a public member
functions overload the pre-decrement ( -- )
operator, for the Rational class.*/
Rational& Rational::operator -- (){
	num -= den;
	return *this;
}
/*  These two public member functions overload the post-increment (++) and post-
decrement ( -- ) operators, respectively, for the Rational class. Note: To
differentiate between the pre- and post- versions of the operators ( ++ ) and ( -
- ), an unused argument is included in the post-versions of these operators.*/
Rational Rational::operator++(int unused){
	unused  = 0;
	Rational flag = *this;
	num += den;
	return flag;

}
Rational Rational::operator--(int unused){
	unused  = 0;
	Rational flag = *this;
	num -= den;
	return flag;
}


/* This friend function overloads the stream extraction operator
( >> ) for the Rational class. It can be used to read the Rational
number r from the input stream is, where each Rational number is
given on a separate line with its three arguments: numerator,
separator ( / ), and denominator, separated one or more white spaces.
If a line in is does not contain a valid Rational number, it prints
out an error message on stderr but it doesn’t stop the execution of
the program.  

*/
istream& operator >> ( istream& is, Rational& r ){
	int p,flag=0,q=1;char c='/';
 	string input;
 	getline(is,input);
 	istringstream iss(input);
	iss>>p>>c>>q>>flag;
 	if (c!='/' || flag != 0){
 		cerr <<"Error: line \""<<input<<"\" contains invalid number"<<endl;
 		return is;
 	}
    r = Rational(p, q);
	return is;
}
/* This friend function overloads the stream insertion operator
( << ) for the Rational class. It can be used to print out the
Rational number r with the numerator num and denominator den as
num/den on the output stream os, and if den = 1, it only prints
out num. */
ostream& operator<< ( ostream& os, const Rational& r ){
	if (r.den==0)
		return os<< "Error: division by zero";
	r.num==0?os<<r.num:r.den == 1?os << r.num:os<<r.num <<"/"<<r.den;
	return os;
}
