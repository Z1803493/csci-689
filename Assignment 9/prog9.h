/******************************************************
CSCI 689 - Assignment 9 - Semester (Fall) 2016
Program:        prog9.h
Author:         Patlolla, Akhil Reddy
Zid:            z1803493
Section:        1
Instructor:     Ibrahim Onyuksel
TA:             Anthony Schroeder
TA:             Avinash Chirumamilla
Date Due:       Friday, November 4, 2016
Purpose:        A header file for implementing N Queens
*******************************************************/
#ifndef PROG_H
#define PROG_H

#include <iostream>
#include <vector>
#include <string>
#include <string>
using namespace std;

/* This is the main method to solve the problem which take 
value of size and create the board and call print when done
 */
void solveNQueens(const unsigned& );	
/* This is a recursive method which make call to the queen location
for every call it goes on backtracking
 */
bool solveNQueensUtil(vector< vector <bool> >&, const unsigned&);	
/*  This method check for the position weather if is safe or not 
to place the queen in the position 
*/
bool isSafe(const vector< vector <bool> >&, const int &, const int &);
/* This method initialize the board by creating the whole vector  */
void initBoard(vector< vector <bool> >&);	
/* This menthod prints the whole board */
void printBoard(const vector< vector<bool> >&);	

#endif