/******************************************************
CSCI 689 - Assignment 9 - Semester (Fall) 2016
Program:        Prog9.cc
Author:         Patlolla, Akhil Reddy
Zid:            z1803493
Section:        1
Instructor:     Ibrahim Onyuksel
TA:             Anthony Schroeder
TA:             Avinash Chirumamilla
Date Due:       Friday, November 4, 2016
Purpose:        C++ program to implement N Queens prob
*******************************************************/
#include "prog9.h" 

int main(){
    for (unsigned N = 1; N < 9; ++N){
        cout << endl << "Board Size = " << N << endl;
        solveNQueens(N);
    }
    return 0;
}
/* This is the main method to solve the problem which take 
value of size and create the board and call print when done
 */
void solveNQueens(const unsigned& n){
    vector<vector<bool> > board(n, vector<bool>(n));
    initBoard(board);
    if (solveNQueensUtil(board, 0))printBoard(board);
    else cout << "Solution does not exist." << endl;
}

/* This is a recursive method which make call to the queen location
for every call it goes on backtracking
 */
bool solveNQueensUtil(vector<vector<bool> >& board, const unsigned& row){
    unsigned sz = board.size();
    unsigned col = rand() % (sz - 0);

    if (row >= sz)return true;

    else if (row < sz){
        unsigned x = col;
        while (x <= sz){
            if (x < sz){
                if (isSafe(board, row, x)){
                    board[row][x] = true;
                    if (solveNQueensUtil(board, row + 1))return true;
                    board[row][x] = false;
                }
            }
            if (x == sz){
                unsigned y = 0;
                while (y <= col){
                    if (isSafe(board, row, y)){
                        board[row][y] = true;
                        if (solveNQueensUtil(board, row + 1))return true;
                        board[row][y] = false;
                    }
                    y++;
                }
            }
            x++;
        }
    }
    return false;
}
/*  This method check for the position weather if is safe or not 
to place the queen in the position 
*/
bool isSafe(const vector<vector<bool> >& board, const int& row, const int& col){
    int sz = board.size();
    for (int x = 0; x < sz; x++)
        for (int y = 0; y < sz; y++)
            if ((abs(row - x) == abs(col - y)) || x == row || y == col)
                if (board[x][y] == true)return false;
    return true;
}
/* This method initialize the board by creating the whole vector  */
void initBoard(vector<vector<bool> >& board){
    int seed = time(0);
    unsigned sz = board.size();
    for (unsigned x = 0; x < sz; x++)
        for (unsigned y = 0; y < sz; y++)
            board[x][y] = false;
    srand(seed);
}
/* This menthod prints the whole board */
void printBoard(const vector<vector<bool> >& board){
    unsigned i,j,grid_size = board.size();  
    string edges = " -----", mid_part = "|     ", queen = "|  Q  ";
    for (j = 0; j < grid_size; ++j){
        for (i = 0; i < grid_size; ++i){
            cout << edges;
            if (i == (grid_size - 1))cout << " ";
        }
        cout << endl;
        for (i = 0; i < grid_size; ++i){
            cout << mid_part;
            if (i == (grid_size - 1))cout << "|";
        }
        cout << endl;
        for (i = 0; i < grid_size; ++i){
            if (board[j][i] == true)cout << queen;
            else
                cout << mid_part;
            if (i == (grid_size - 1))cout << "|";
        }
        cout << endl;
        for (i = 0; i < grid_size; ++i){
            cout << mid_part;
            if (i == (grid_size - 1))cout << "|";
        }
        cout << endl;
        if (j == (grid_size - 1)){
            for (i = 0; i < grid_size; ++i){
                cout << edges;
                if (i == (grid_size - 1))cout << " ";
            }
        }
    }
    cout << endl;
}



