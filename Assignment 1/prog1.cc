/******************************************************
CSCI 689 - Assignment 1 - Semester (Fall) 2016
Program:        prog1.cc
Author:         Patlolla, Akhil Reddy
Zid:            z1803493
Section:        1
Instructor:     Ibrahim Onyuksel
TA:             Anthony Schroeder
TA:             Avinash Chirumamilla
Date Due:       Monday, August 29, 2016
Purpose:        This program accepts a single number
                representing number of odd numbers and
                give sum on those number of odd numbers.
*******************************************************/
#include "prog1.h"

int main(){
        int n,s=0;
        cin>>n;
        for (int i = 0; i < 2*n; i++){
                if (isEven(i) != true){
                        s = s + i;
                }
        }
        cout<<n<<" "<<s<<endl;
        return 0;
}

//Simple method to calcualte Even number
bool isEven(int i){
        if ( i%2 == 0)
                return true;
        else
                return false;

}
