/******************************************************
CSCI 689 - Assignment 11 - Semester (Fall) 2016
Program:        Shape3.cc
Author:         Patlolla, Akhil Reddy
Zid:            z1803493
Section:        1
Instructor:     Ibrahim Onyuksel
TA:             Anthony Schroeder
TA:             Avinash Chirumamilla
Date Due:       Monday, November 28, 2016
Purpose:        Abstract implementation
*******************************************************/
#include "/home/cs689/progs/16f/p11/Shape2.h"
#include "/home/cs689/progs/16f/p11/Shape3.h"
#define PI ( 4 * atan ( 1 ) )

Box::Box(const double& l,const double& w, const double& h){
    length=l;width=w;height=h;
}
// Box copy constructor
Box::Box( const Box & d): Rectangle(){
    length=d.length;width=d.width;height=d.height;
}
// assignment operator overloading
Box& Box::operator=(const Box& d){
    length = d.length;width = d.width;
    height=d.height;return *this;
}
//append operator overload 
Box& Box::operator+=(const Box& d){
    length = length+d.length;width = width+d.width;
    width = height +d.height;return *this;
}
Box::~Box(){}
//function body to find area of a box
double  Box::area()const{
    double ar = Rectangle::area();
    double p = Rectangle::perimeter();
    double f=2;
    return (f*ar)+(height*p);
}
double Box::perimeter() const{
    return 0;
}
//function body to find volume of a box
double Box::volume()const {
    double ar = Rectangle::area();
    return (ar*height);
}
void Box::print()const{
    cout<<"Length = "<<length<<" Width = "<<width<<" Height = "<<height;
}
Cube::Cube(const double& l){
    length=l;
    width=l;
}
// Cube copy constructor
Cube::Cube( const Cube & d):Square(){
    length=d.length;
    width=d.width;
}
// assignment operator overloading
Cube& Cube::operator=(const Cube& d){
    length = d.length;
    width = d.width;
    return *this;
}
// append operator overloading
Cube& Cube::operator+=(const Cube& d){
    length = length+d.length;
    width = width+d.width;
    return *this;
}
Cube::~Cube(){}
//function body to find area of a cube
double  Cube::area()const   {
    double ar = Rectangle::area();
    double f=6;
    return f*ar;
}
double Cube::perimeter() const{
    return 0;
}
//function body to find volume of a cube
double Cube::volume()const{
    double ar = Rectangle::area();
    return length*ar;
}
void Cube::print()const{
    cout<<"Length ="<<length;
}
Cylinder::Cylinder(const double& r,const double& h){
	radius=r;	height=h;
}
// Cylinder copy constructor
Cylinder::Cylinder( const Cylinder & d):Circle(){
    radius=d.radius;    height=d.height;
}
// assignment operator overloading
Cylinder& Cylinder::operator=(const Cylinder& d){
    radius=d.radius;
    height=d.height;
    return *this;
}
// append operator overloading
Cylinder& Cylinder::operator+=(const Cylinder& d){
    radius=radius+d.radius;
    height=height+d.height;
    return *this;
}
Cylinder::~Cylinder(){}
//function body to find area of a cylinder
double  Cylinder::area()const {
    double ar = Circle::area();
    double p = Circle::perimeter();
    double f=2;
    return (f*(ar)+(p*height));
}
double Cylinder::perimeter() const{
    return 0;
}
//function body to find volume of a cylinder
double Cylinder::volume()const  {
    double ar = Circle::area();
    return (ar*height);
}
void Cylinder::print()const{
    cout<<"Radius ="<<radius<<"Height ="<<height;
}
Cone::Cone(const double& r,const double& h){
	radius=r;	height=h;
}
//Cone copy constructor
Cone::Cone( const Cone & d):Circle(){
    radius=d.radius;
    height=d.height;
}
// assignment operator overloading
Cone& Cone::operator=(const Cone& d){
    radius=d.radius;
    height=d.height;
    return *this;
}
// append operator overloading
Cone& Cone::operator+=(const Cone& d){
    radius=radius+d.radius;
    height=height+d.height;
    return *this;
}
Cone::~Cone(){}
//function body to find area of a cone
double  Cone::area()const{
    double ar = Circle::area();
    double p = PI*radius*(sqrt((height*height)+(radius*radius)));
    return  ar+p;
}
double Cone::perimeter() const{
    return 0;
}
//function body to find volume of a cone
double Cone::volume()const {
    double ar = Circle::area();
    double f=3;
    return (height * ar)/f;
}
void Cone::print()const{
    cout<<"Radius = "<<radius<<" Height = "<<height;
}
Sphere::Sphere(const double& r){
	radius=r;
}
// Sphere copy constructor
Sphere::Sphere( const Sphere & d):Circle(){
    radius=d.radius;
}
// assignment operator overloading
Sphere& Sphere::operator=(const Sphere& d){
    radius=d.radius;
    return *this;
}
// append operator overloading
Sphere& Sphere::operator+=(const Sphere& d){
    radius=radius+d.radius;
    return *this;
}
Sphere::~Sphere(){}
//function body to find area of a sphere
double  Sphere::area()const{
    double ar = Circle::area();
    double f=4;
    return f*ar;
}
//function body to find volume of a sphere
double Sphere::volume()const{
    double ar = Circle::area();
    double f1=4;
    double f=3;
    return (f1*radius*ar)/f;
}
Tetrahedron::Tetrahedron(const double& tetra):equTriangle(tetra){}
// Tetrehedron copy constructor
Tetrahedron::Tetrahedron( const Tetrahedron & d):equTriangle(){
    a=d.a;b=d.b;c=d.c;
}
// assignment operator overloading
Tetrahedron& Tetrahedron::operator=(const Tetrahedron& d){
    a = d.a;b = d.b;c=d.c;
    return *this;
}
// append operator overloading
Tetrahedron& Tetrahedron::operator+=(const Tetrahedron& d){
    a = a+d.a;    b = b+d.b;c= c+d.c;
    return *this;
}
Tetrahedron::~Tetrahedron(){}
//function body to find area of a tetrahedron
double  Tetrahedron::area()const    {
    double ar = equTriangle::area();
    double f=4;
    return f*ar;
}
//function body to find area of a tetrahedron
double Tetrahedron::perimeter() const{
    return 0;
}
//function body to find volume of a tetrahedron
double Tetrahedron::volume()const{
    double ar = equTriangle::area();
    double f1=2;
    double f=3;
    double h= sqrt(f1/f)*a;
    return (h*ar)/f;
}