#include "Shape3.h"

#ifndef H_PROG11
#define H_PROG11

#include <iomanip>
#include <string>

#define N 50
#define LINE string ( N, '=' )

void Print ( const string&, const Shape* );

#endif
