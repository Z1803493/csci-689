/******************************************************
CSCI 689 - Assignment 11 - Semester (Fall) 2016
Program:        Shape2.cc
Author:         Patlolla, Akhil Reddy
Zid:            z1803493
Section:        1
Instructor:     Ibrahim Onyuksel
TA:             Anthony Schroeder
TA:             Avinash Chirumamilla
Date Due:       Monday, November 28, 2016
Purpose:        Abstract implementation
*******************************************************/
#include "/home/cs689/progs/16f/p11/Shape2.h"
#define PI ( 4 * atan ( 1 ) )

Rectangle::Rectangle(const double &l,const double &w):length(l),width(w){}
//Copy Constructor
Rectangle::Rectangle( const Rectangle & d){
    length=d.length;
    width=d.width;
}
//Operator Overloading
Rectangle& Rectangle::operator=(const Rectangle& d){
    length = d.length;width = d.width;
    return *this;
}
Rectangle& Rectangle::operator+=(const Rectangle& d){
    length = length+d.length;
    width = width+d.width;
    return *this;
}
//Area
double Rectangle:: area() const{
    return (length*width);
}
//Perimeter
double Rectangle::perimeter() const {
    return 2*(length+width);
}
//Printing
void Rectangle::print() const{
    cout <<"length = "<<length<<" : "<<"width = "<<width;
}
Rectangle::~Rectangle(){}
Square::Square(const double& s){
length=s;width=s;
}
//Copy Constructor
Square::Square( const Square & d): Rectangle(){
    length=d.length;width=d.width;
}
//Operator Overloading
Square& Square::operator=(const Square& d){
    length = d.length;width = d.width;
    return *this;
}
Square& Square::operator+=(const Square& d){
    length = length+d.length;width = width+d.width;
    return *this;
}
Square::~Square(){}
//Printing
void Square::print() const{
    cout<<"Length = "<<length;
}
Circle::Circle(const double &r):radius(r){}
//Copy Constructor
Circle::Circle( const Circle & d){
    radius=d.radius;
}
//operator overloading
Circle& Circle::operator=(const Circle& d){
    radius = d.radius;
    return *this;
}
Circle& Circle::operator+=(const Circle& d){
    radius = radius+d.radius;
    return *this;
}
Circle::~Circle(){}
//Area
double Circle::area() const{
    return (PI*radius*radius);
}
//Perimeter
double Circle:: perimeter() const {
    return  2*(PI*radius);
}
//Printing
void Circle::print() const{
    cout <<"Radius ="<<radius;
}
Triangle:: Triangle(const double& a,const double& b,const double& c):a(a),b(b),c(c){}
//copy constructor
Triangle::Triangle( const Triangle & d){
    a=d.a;b=d.b;c=d.c;
}
//operator overload
Triangle& Triangle::operator=(const Triangle& d){
    a=d.a;b=d.b;c=d.c;
    return *this;
}
Triangle& Triangle::operator+=(const Triangle& d){
    a=a+d.a;b=b+d.b;c=c+d.c;
    return *this;
}
Triangle::~Triangle(){}
double Triangle::area() const{
    double p = a+b+c;    double k=p/2;
    return sqrt(k*(k-a)*(k-b)*(k-c));
}
//Perimeter
double Triangle::perimeter() const{
    return a+b+c;
}
//Printing
void Triangle::print() const {
    cout <<"a ="<<a<<":"<<"b ="<<b<<"c ="<<c;
}
rightTriangle::rightTriangle(const double& rx,const double& ry){
	a=rx; b=ry;
	c=sqrt(rx*rx+ry*ry);
}
//copy constructor
rightTriangle::rightTriangle( const rightTriangle & d):Triangle(){
    a=d.a;b=d.b;c=d.c;
}
//operator overload
rightTriangle& rightTriangle::operator=(const rightTriangle& d){
    a=d.a;b=d.b;c=d.c;
    return *this;
}
rightTriangle& rightTriangle::operator+=(const rightTriangle& d){
    a=a+d.a;b=b+d.b;c=c+d.c;
    return *this;
}
rightTriangle::~rightTriangle(){}
void rightTriangle::print() const{
    cout<<"Length = "<<a<<" Height = "<<b;
}
equTriangle::equTriangle(const double& eq){
	a=b=c=eq;		
}
//Copy Constructor
equTriangle::equTriangle( const equTriangle & d):Triangle(){
    a=d.a;b=d.b;c=d.c;
}
//operator overloading
equTriangle& equTriangle::operator=(const equTriangle& d){
    a=d.a;b=d.b;c=d.c;
    return *this;
}
equTriangle& equTriangle::operator+=(const equTriangle& d){
    a=a+d.a;b=b+d.b;c=c+d.c;
    return *this;
}
equTriangle::~equTriangle(){}
void equTriangle::print() const{
    cout<<"Length = "<<a;
}