/******************************************************
CSCI 689 - Assignment 5 - Semester (Fall) 2016
Program:        prog5.h
Author:         Patlolla, Akhil Reddy
Zid:            z1803493
Section:        1
Instructor:     Ibrahim Onyuksel
TA:             Anthony Schroeder
TA:             Avinash Chirumamilla
Date Due:       Friday , September 30, 2016
Purpose:        A header file for The Game of Minesweeper
*******************************************************/
#ifndef PROG_H
#define PROG_H


#include <iostream>
#include <string.h>
#include <vector>
using namespace std;
const int SEED = 1;
/***************************
Function to build mine field
***************************/
void buildMineField ( vector < vector < bool > >& mines, const double& mineProb );
/***************************
Function to display mine locations
***************************/
void displayMineLocations ( const vector < vector < bool > >& mines );
/***************************
Function to display mine count
***************************/
void displayMineCounts ( const vector < vector < unsigned > >& counts );
/***************************
Function to display count 
***************************/
void fixCounts ( const vector < vector < bool > >& mines, vector < vector < unsigned >>& counts );

#endif

