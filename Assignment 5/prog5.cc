/******************************************************
CSCI 689 - Assignment 5 - Semester (Fall) 2016
Program:        prog5.h
Author:         Patlolla, Akhil Reddy
Zid:            z1803493
Section:        1
Instructor:     Ibrahim Onyuksel
TA:             Anthony Schroeder
TA:             Avinash Chirumamilla
Date Due:       Friday , September 30, 2016
Purpose:        A program for The Game of Minesweeper
				showing files
*******************************************************/
#include "prog5.h"

int main(){
	int size_vector;
    double mineProb ;
    while (cin >> size_vector && cin >> mineProb) {
		cout<<"Mine Locations: size = "<<size_vector<<" x "<<size_vector<<" , prob of mine = "<<mineProb<<endl;
    	vector < vector < bool > > mine(size_vector, vector < bool > (size_vector));	
    	buildMineField(mine, mineProb);
    }
    return 0;
}
/****************************
Function to build mine field
****************************/
void buildMineField(vector < vector < bool > >& mine, const double& mineProb){
    srand(SEED);
    for (unsigned flag = 0; flag < mine.size(); flag++) {
        for (unsigned flag2 = 0; flag2 < mine[flag].size(); flag2++) {
            double rnd = (rand() / (double(RAND_MAX) + 1));//rand generation
            if (rnd < mineProb) mine[flag][flag2] = 1;
            else mine[flag][flag2] = false;
        }
    }
    displayMineLocations(mine);
}
/***************************
Function to display mine locations
***************************/
void displayMineLocations(const vector<vector<bool> >& mine){

    string horizontal_string = "____";//line pattern
    for (unsigned flag = 0; flag != mine.size(); flag++) {
        for (unsigned flag2 = 0; flag2 < mine.size(); flag2++) cout << horizontal_string;// above line
        cout << endl;
        for (unsigned flag3 = 0; flag3 != mine[flag].size(); flag3++) {
            cout << "| ";//sides
            if (mine[flag][flag3] == 1) cout << "X ";//display mines 
            else cout << "  ";//empty spaces
        }cout << "| " << endl;//closing edges
    }
    for (unsigned flag2 = 0; flag2 < mine.size(); flag2++) cout << horizontal_string;
    vector < vector < unsigned > > counts;
    fixCounts(mine, counts);
}
/***************************
Function to display count 
***************************/
void fixCounts(const vector < vector < bool > >& mine, vector < vector < unsigned > >& counts){
    int size = mine.size();
    counts.resize(size);
    for (int flag = 0; flag < size; flag++) {
        for (int flag2 = 0; flag2 < size; flag2++) { //conitions for checking mines in all sides
            counts[flag].push_back(0);
            if (mine[flag][flag2] == 1)	counts[flag][flag2]++;
            if (((flag + 1) < size) && (mine[flag + 1][flag2] == 1))	counts[flag][flag2]++;
            if ((((flag + 1) < size) && (flag2 + 1 < size)) && (mine[flag + 1][flag2 + 1] == 1))	counts[flag][flag2]++;
            if (((flag - 1 >= 0) && ((flag2 - 1) >= 0)) && (mine[flag - 1][flag2 - 1] == 1))	counts[flag][flag2]++;
            if ((((flag + 1) < size) && ((flag2 - 1) >= 0)) && (mine[flag + 1][flag2 - 1] == 1))	counts[flag][flag2]++;
            if (((flag2 - 1) >= 0) && (mine[flag][flag2 - 1] == 1))	counts[flag][flag2]++;
            if (((flag2 + 1) < size) && (mine[flag][flag2 + 1] == 1))	counts[flag][flag2]++;
            if ((((flag - 1) >= 0) && ((flag2 + 1) < size)) && (mine[flag - 1][flag2 + 1] == 1))	counts[flag][flag2]++;
            if (((flag - 1) >= 0) && (mine[flag - 1][flag] == 1))	counts[flag][flag2]++;
        }cout << endl;
    }
    displayMineCounts(counts);
}
/***************************
Function to display mine count
***************************/
void displayMineCounts(const vector < vector < unsigned > >& counts){
	cout<<"Mine Counts:"<<endl;
    string  horizontal_string= "____"; // line pattern
    for (unsigned flag = 0; flag < counts.size(); flag++) {
        for (unsigned flag2 = 0; flag2 < counts.size(); flag2++) cout << horizontal_string;// above line
        cout << endl;
        for (unsigned flag3 = 0; flag3 < counts[flag].size(); flag3++) {
            cout << "| "; //sides
            cout << counts[flag][flag3] << " ";//inside values
        }cout << "| " << endl;//closing edges
    }
    for (unsigned flag2 = 0; flag2 < counts.size(); flag2++) cout << horizontal_string; // lower line
    cout << endl;
}
