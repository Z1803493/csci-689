/******************************************************
CSCI 689 - Assignment 10 - Semester (Fall) 2016
Program:        prog10.cc
Author:         Patlolla, Akhil Reddy
Zid:            z1803493
Section:        1
Instructor:     Ibrahim Onyuksel
TA:             Anthony Schroeder
TA:             Avinash Chirumamilla
Date Due:       Tuesday, November 15, 2016
Purpose:        A calculater implementation
*******************************************************/


#include "prog10.h"

int main()
{
    string str;
    stack<double> S;
    while (getline(cin, str)) {
        istringstream isstr(str);
        string token;
        while (getline(isstr, token, ' ')) {
            process_token(token, S);
        }
    }
    return 0;
}

void process_token(const string& token, stack<double>& S)
{
    bool Unflg;
    bool float_Flag;
    bool lamo_flag;
    lamo_flag = false;
    char s;
    char sign;
    double n, total, a, b;
    for (unsigned i = 0; i < token.length(); i++) {
        if (i != (token.length() - 1)) {
            Unflg = unarySign(token[i], token, i);
            if (Unflg)
                s = token[i];

            if (token[i] == '.') {
                float_Flag = floatPoint(token[i], token, i);
                if (float_Flag) {
                    i++;
                }
            }
        }

        if (isdigit(token[i])) {
            n = getNumber(token, i, float_Flag);
            if (s == '-') {
                n = (n) * (-1);
                S.push(n);
            }
            else
                S.push(n);
        }

        if (!Unflg) {
            lamo_flag = isValidOperator(token[i]);
            if (lamo_flag)
                sign = token[i];
        }

        if (lamo_flag) {
            a = popStack(S);
            b = popStack(S);
            total = operation(sign, a, b);
            S.push(total);
        }

        if (token[i] == '=')
            printResult(S);

        if ((!isdigit(token[i])) && (!Unflg) && (tolower(token[i] != 'c')) && (!lamo_flag) && (token[i] != '='))
            cerr << "error: "
                 << "'" << token[i] << "'"
                 << " is invalid" << endl;

        if (tolower(token[i]) == 'c')
            emptyStack(S);
    }
}

void emptyStack(stack<double>& S)
{
    while (!S.empty()) {
        S.top();
        S.pop();
    }
}

void printResult(const stack<double>& S)
{
    if (S.empty()) {
        cerr << "Error : Empty Stack " << endl;
        exit(EXIT_FAILURE);
    }
    else {
        S.top();
        cout << fixed << showpoint << setprecision(2) << setw(10) << S.top() << endl;
    }
}

double popStack(stack<double>& S)
{
    double topmost;
    if (S.empty()) {
        cerr << "error : Stack is empty " << endl;
        return 0;
    }
    else {
        topmost = S.top();
        S.pop();
    }
    return topmost;
}

double operation(const char& c, const double& x, const double& y)
{
    switch (c) {
    case '+': return x + y;
    case '-': return y - x;
    case '*': return x * y;
    case '/': return y / x;
    }
}

bool isValidOperator(const char& c)
{
    if (c == '+' || c == '-' || c == '*' || c == '/')
        return true;
        return false;
}

bool floatPoint(const char& c, const string& token, const unsigned& i)
{
    if ((c == '.') && (isdigit(token[i + 1])))
        return true;
    else
        return false;
}

bool unarySign(const char& c, const string& token, const unsigned& i)
{
    if ((c == '+' || c == '-') && (isdigit(token[i + 1]) || token[i + 1] == '.'))
        return true;
    else
        return false;
}

double getNumber(const string& token, unsigned& i, const bool& floatPointFlag)
{
    string str;
    double num;
    int start = i, n = 0, count = 0, p = 0;
    n++;
    i++;
    while ((i <= token.length() - 1) && (isdigit(token[i]) || token[i] == '.')) {
        n++;
        i++;
    }
    i--;
    while (p < n) {
        if (token[p] == '.')
            count++;
        p++;
    }
    if (count > 1) {
        str = token.substr(start, n);
        cerr << "error : "
             << "'" << str << "'"
             << "More than 1 decimal point, not valid" << endl;
        exit(EXIT_FAILURE);
    }
    else
        str = token.substr(start, n);
    num = stringToDouble(str);
    if (floatPointFlag) {
        num = (num / pow(10, n));
    }
    return num;
}

double stringToDouble(string str)
{
    double n;
    stringstream ss;
    ss<<str;
    ss>>n;
    return n;
}

