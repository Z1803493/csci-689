/******************************************************
CSCI 689 - Assignment 10 - Semester (Fall) 2016
Program:        prog10.h
Author:         Patlolla, Akhil Reddy
Zid:            z1803493
Section:        1
Instructor:     Ibrahim Onyuksel
TA:             Anthony Schroeder
TA:             Avinash Chirumamilla
Date Due:       Tuesday, November 15, 2016
Purpose:        A calculater header
*******************************************************/


#ifndef PROG_H 
#define PROG_H

#include <iostream> // Library for standard input/output functions
#include <stack>    // Library for stacks 
#include <vector>   // Library for vectoes
#include <string>   // Library for strings
#include <fstream>  // Library for istream/ostream
#include <iomanip>  // Library for manipulations 
#include <math.h>   // Library for math functions
#include <sstream>  // Library for input/output stream functions



using namespace std;

typedef basic_istringstream<char> istringstream;

void process_token(const string& token,stack <double>& S);
bool unarySign(const char& c, const string& token, const unsigned& i);
bool floatPoint(const char& c, const string& token, const unsigned& i);
double getNumber(const char& c, const string& token, unsigned& i, const bool& floatPointFlag);
bool isValidOperator(const char& c);
double getNumber(const string&, unsigned int&, const bool&);
double operation(const char&c, const double& x, const double& y);
double popStack( stack <double>& S);
void printResult(const stack <double>& S);
void emptyStack(stack <double>& S);

double stringToDouble(string str);
#endif


