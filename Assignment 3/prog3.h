/******************************************************
CSCI 689 - Assignment 3 - Semester (Fall) 2016
Program:        prog3.h
Author:         Patlolla, Akhil Reddy
Zid:            z1803493
Section:        1
Instructor:     Ibrahim Onyuksel
TA:             Anthony Schroeder
TA:             Avinash Chirumamilla
Date Due:       Wednesday, September 14, 2016
Purpose:        Header file for the program to encode
				give data as Caesar Cipher
*******************************************************/
#ifndef PROG_H
#include <iostream>		/*Input & Output Sream */
#include <fstream>		/*perform operations over File Stream*/
#include <string>		/*Perform operations on String*/
using namespace std;

/*Constant string value over path of the data file*/
const string path_data="/home/cs689/progs/16f/p3/prog3.d2";
/* */
const int alpha_count = 26;
/******************************************************
Prototype for the function which take shift as input
and call encodeCaesarCipher capturing the date from the
file of the stream.
******************************************************/
void process_infile ( int shift );
/******************************************************
Prototype for the function which takes string from file
and shift value process_infile and call new_position
by sending each char and shift value which return an
int value which intern converts to char and add to the
encrypted string.
******************************************************/
string encodeCaesarCipher ( string str, int shift );
/******************************************************
Protype for the function which takes a char and shift
value as input and process the Caesar Cipher encoding
and return the int value of the encoded character.
******************************************************/
int new_position ( char c, int shift );

#endif
