/******************************************************
CSCI 689 - Assignment 3 - Semester (Fall) 2016
Program:        prog3.cc
Author:         Patlolla, Akhil Reddy
Zid:            z1803493
Section:        1
Instructor:     Ibrahim Onyuksel
TA:             Anthony Schroeder
TA:             Avinash Chirumamilla
Date Due:       Wednesday, September 14, 2016
Purpose:        A Simple Program implementing encoding
		for Caesar Cipher based in the given
		shift value and taking input from data.
*******************************************************/
#include "prog3.h"

int main()
{
    /* Variable to store the shift value */
    int shift;
    while (cin >> shift) {
        /*calling the function passing the shift value to
		the function */
        process_infile(shift);
    }
}
/******************************************************
Defination for the function which take shift as input
and call encodeCaesarCipher capturing the date from the
file of the stream.
******************************************************/
void process_infile(int shift)
{
    /* A String variable to store read from data file
	and pass the encodeCaesarCipher function*/
    string str;
    cout << "shift = " << shift << endl;
    /* Declaring the file Stream and accessing the data*/
    ifstream data_file(path_data);
    /* Check weather the file is opened or not to confirm
	failure case*/
    if (data_file.fail()) {
        /* In case of failure in opening the file keeping
		the error message in the cerr stream*/
        cerr << "Error while opening the file" << endl;
        /* Exit by give status*/
        exit(EXIT_FAILURE);
    }
    /* To read the file and pass string to encodeCeasarCipher
	method till end of file*/
    while (!data_file.eof()) {
        /* Read the lion and store in the string */
        getline(data_file, str);
        /* A funciton which returns the encoded string and store
		in another string value to print on output stream */
        string encryptedString = encodeCaesarCipher(str, shift);
        /* Printing the output */
        cout << encryptedString << endl;
    }
}
/******************************************************
Defination for the function which takes string from file
and shift value process_infile and call new_position
by sending each char and shift value which return an
int value which intern converts to char and add to the
encrypted string.
******************************************************/
string encodeCaesarCipher(string str, int shift)
{
    /* An empty string to store encrypted string*/
    string encryptedString = "";
    /* A loop to read each char of the string  */
    for (int i = 0, n = str.length(); i < n; i++) {
        /* to check weather the read character is an
		alphabet or not*/
        if (isalpha(str[i])) {
            /* passing the char from the string to
			new_position method which return an int
			value of the encrypted char*/
            int x = new_position((char)str[i], shift);
            /* add to the main encryted string by
			converting int value to char*/
            encryptedString = encryptedString + (char)x;
        }
        else {
            /* if read character is not alphabet then add
			to the encrypted string */
            char y = str[i];
            encryptedString = encryptedString + y;
        }
    }
    /*returnt he encrypted string  */
    return encryptedString;
}
/******************************************************
Defination for the function which takes a char and shift
value as input and process the Caesar Cipher encoding
and return the int value of the encoded character.
******************************************************/
int new_position(char c, int shift)
{
    /*In case of shift is above alpha count then to a
	balanced shift in range of 26  */
    if (shift > alpha_count) {
        shift = shift % alpha_count;
    }
    /* In case of shift with a negative value and absolute
	value below alpha count then to a balanced shift in
	range of 26  */
    else if (shift < -alpha_count) {
        shift = (alpha_count + (shift % alpha_count));
    }
    /*In case of shift with a negative value and absolute
	value above alpha count then to a balanced shift in
	range of 26 */
    else if (shift < 0 && shift > -alpha_count) {
        shift = alpha_count + shift;
    }
    /* A flag to evaluate the char is caps or small*/
    int flag_capsORsmall = 65;
    /* in case of lower letter have to change the value of the
	flagcapsORsmall value */
    if (islower(c))
        flag_capsORsmall = 97;
    /* expression which is heart of the whole encoding */
    int cipheredLetter = (((int)c - flag_capsORsmall + shift) % alpha_count) + flag_capsORsmall;
    /* return the int value of the encoded text*/
    return cipheredLetter;
}
