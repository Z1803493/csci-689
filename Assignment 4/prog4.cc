/******************************************************
CSCI 689 - Assignment 4 - Semester (Fall) 2016
Program:        prog4.cc
Author:         Patlolla, Akhil Reddy
Zid:            z1803493
Section:        1
Instructor:     Ibrahim Onyuksel
TA:             Anthony Schroeder
TA:             Avinash Chirumamilla
Date Due:       Wednesday, September 14, 2016
Purpose:        A program which take a c++ program and
		remove all the comments and make a new
		c++ program


*******************************************************/
#include "prog4.h"

int main()
{
    /* Declaring an input stream to read the file */
    ifstream is;
    /* Declaring an output stream to store the processed file */
    ofstream os;
    /* Function call passing streams to open file from respective streams */
    open_files(is, os);
    /* Function call passing streams to proces file and push processed data
	to output stream */
    process_data(is, os);
    /* Function call passing streams to close the opened file using those streams */
    close_files(is, os);
    return 0;
}

/**************************************************
Definition for the function, which takes both input
and output streams as arguments and open the files in
those streams respectively
**************************************************/
void open_files(ifstream& is, ofstream& os)
{

    /* using open() from ios over input stream so passing path
	of the input file and mode to open*/
    is.open(path_ip, ios::in);
    /* using open() from ios over output stream so passing path
	of the output file and mode to open*/
    os.open(path_op, ios::out);
    /*	Error checking weather the file opening is done successfully */
    if (is.fail() || os.fail()) {
        /* In case of failure in opening the file keeping
		the error message in the cerr stream*/
        cerr << "Error while opening the file" << endl;
        /* Exit by give status*/
        exit(EXIT_FAILURE);
    }
    return;
}

/**************************************************
Definition for the function, which takes both input
and output streams as arguments and process the
conditions and pull out all the comments and push
content which are not part of comments into the
output stream which in-turn creates program without
any comments
**************************************************/
void process_data(ifstream& is, ofstream& os)
{
    /* A flag value to identify the case of the switch and
	roll over a new line and move searching for the comments*/
    int flag_optional = 0;
    /* An infinite loop to check till the end of the file  */
    while (!is.eof()) {
        /* A character to store the value of the char */
        char c;
        /* A get function to read char from input stream*/
        is.get(c);
        /* condition over the loop to break */

        int exception_op = 0;
    /* A switch case for all evaluating cases */
    cur_newline:
        switch (flag_optional) {

        /* For every new line it evaluates char by car for occurrence of
        	comments and if found any updates the flag value to 1  */
        case 0:
            if (c == '/')
                flag_optional = 1;
            else if (c == '\"') {
                flag_optional = 6;
                exception_op = 1;
            }
            else
                exception_op = 1;
            break;

        /* when flag value is 1 it will check for for occurrence of other
	char of the commend and evaluates for the continuation and start
	and effect updating the output file */

        case 1:
            if (c == '/')
                flag_optional = 2;
            else if (c == '*')
                flag_optional = 3;
            else {
                exception_op = 2;
                flag_optional = 0;
            }

        /* when the flag is 2 then it will check for the occurrence of the
	end of line and work with the sectional aspects with a break
	changing value to go and print the char to output as exception_op
	is 1 and changing flag to 4 */
        case 2:
            if (c == '\n' || c == '\r') {
                c = '\n';
                exception_op = 1;
                flag_optional = 4;
            }
            break;
        /* this check for a asterix and changes the flag to 5 for next char section   */
        case 3:
            if (c == '*')
                flag_optional = 5;
            break;
        /* chekcing for new line and return escape sequence */
        case 4:
            if (c != '\n' && c != '\r') {
                flag_optional = 0;
                goto cur_newline;
            }
            break;
        /* iterative section for the case of multiple astrix char and also
	 backslash with a break to classify the state of flag optional*/
        case 5:
            if (c == '/')
                flag_optional = 0;
            else if (c == '*') {
                flag_optional = 5;
            }
            else
                flag_optional = 3;
            break;
        /* classifying the exception fir multiple comments with in program
			which match the pattern but not a comment */
        case 6:
            if (c == '\"')
                flag_optional = 0;
            else if (c == '\\')
                flag_optional = 7;
            exception_op = 1;
            break;
        /* final case for the to end the finals section of the comments and
	to check for the continuation of the verification of the comments */
        case 7:
            exception_op = 1;
            flag_optional = 6;
            break;
        }
        /* switch case to classify an exception of the comment pattern in between
        the program and to exclude and keep in the stream */
        switch (exception_op) {
        /* case 2 is specified in the top so that it should execute the case 1
			when ever case 2 is true */
        case 2:
            os.put('/');
        /* this case pushes the char into the output stream*/
        case 1:
            os.put(c);
            break;
        }
    }

    return;
}

/**************************************************
Definition for the function, which takes both input
and output streams as arguments and will close the
files opened using those streams
**************************************************/
void close_files(ifstream& is, ofstream& os)
{

    /* closing the input stream */
    is.close();
    /* closing the output stream */
    os.close();
    return;
}

