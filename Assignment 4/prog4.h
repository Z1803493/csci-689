/******************************************************
CSCI 689 - Assignment 4 - Semester (Fall) 2016
Program:        prog4.h
Author:         Patlolla, Akhil Reddy
Zid:            z1803493
Section:        1
Instructor:     Ibrahim Onyuksel
TA:             Anthony Schroeder
TA:             Avinash Chirumamilla
Date Due:       Wednesday, September 22, 2016
Purpose:        Header file for the program to take a
		c++ program as input and give output
		another c++ program removing all the
		comments
*******************************************************/
#ifndef PROG_H

#include <iostream>		/*Input & Output Stream */
#include <fstream>		/*perform operations over File Stream*/
#include <string>		/*Perform operations on String*/
using namespace std;

/*Constant string value over path of the input file*/
const string path_ip="/home/cs689/progs/16f/p4/prog4-in.cc";

/*Constant string value over path of the output file*/
const string path_op="prog4-out.cc";

/**************************************************
A prototype for the function, which takes both input
and output streams as arguments and open the files in
those streams respectively
**************************************************/
void open_files ( ifstream& is, ofstream& os );

/**************************************************
A prototype for the function, which takes both input
and output streams as arguments and process the
conditions and pull out all the comments and push
content which are not part of comments into the
output stream wich inturn creates program without
any comments
**************************************************/
void process_data ( ifstream& is, ofstream& os);

/**************************************************
A prototype for the function, which takes both input
and output streams as arguments and will close the
files opened using those streams
**************************************************/
void close_files ( ifstream& is, ofstream& os );


#endif
